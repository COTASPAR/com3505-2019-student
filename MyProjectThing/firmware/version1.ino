/* 
IoT Project 2020: A.N.A 
Allergy Neutraliser for Asthma - The project aims were to use the unPhone platform to develop an air-dust monitoring system which could turn on/off an air filter owened by Carlo to 
lower the amount of dust in the air in order to tame his allergies. Also, the data should be pushed to "the cloud" so that further analysis of the dust concentrations can be carried out.
1. Power Management Functionality 
2. Radio Control Functionality 
3. Wifi for data logs 
- Anete Zepa & Carlo Pecora-Grisafi
*/

#include <SPI.h>                 // the SPI bus
#include <Adafruit_GFX.h>        // core graphics library
#include "Adafruit_HX8357.h"     // tft display local hacked version
#include <Wire.h>                // I²C comms on the Arduino
#include <IOExpander.h>          // unPhone's IOExpander (controlled via I²C)
#include <GP2Y1010_DustSensor.h> // Library for the Sharp dust sensor
#include <RCSwitch.h>            // Library for the radio controller switch 
#include <Adafruit_Sensor.h>    // base class etc. for sensor abstraction
#include <WiFi.h>

// AP's SSID and Password
const char* ssid     = "Pixel";
const char* password = "12345678";
const char* resource = "/trigger/GP2Y1010_DustSensor/with/key/dM9fXLVxRY4N6Q0tUONW0J";

// Maker Webhooks IFTTT
const char* server = "maker.ifttt.com";

const int DustLEDPin = 32;
const int DustPin = 15;

// power management chip config
byte I2Cadd = 0x6b;      // I2C address of the PMU
byte BM_Watchdog = 0x05; // charge termination/timer control register
byte BM_OpCon    = 0x07; // misc operation control register
byte BM_Status   = 0x08; // system status register 
byte BM_Version  = 0x0a; // vender / part / revision status register 
void checkPowerSwitch(); // what position is the on/off switch in?
void offFilter(float);        //Turning off filter
void onFilter(float);        //Turning off filter
void initWifi(); // initialise wifi
void makeIFTTTRequest(); // make a ifttt request
void setRegister(byte, byte);   // PMU...
byte getRegister(byte);         //    ...I2C...
void write8(byte, byte, byte);  //       ...helper...
byte read8(byte, byte);         //          ...functions
void setShipping(bool);  // tell the PMU to sleep

// the LCD
#define TFT_DC   21      // this isn't on the IO expander
Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, TFT_DC, IOExpander::LCD_RESET);

GP2Y1010_DustSensor dustsensor;
RCSwitch mySwitch = RCSwitch();

void setup() {
  Serial.begin(115200);
  Serial.println("Dust sensor readings");
  mySwitch.enableTransmit(12);
  mySwitch.setPulseLength(175);
  Wire.setClock(100000);
  Wire.begin();
  IOExpander::begin();
  checkPowerSwitch(); // check if the power switch is now off & if so shutdown
  delay(10);
  tft.begin(HX8357D); // start the LCD driver chip
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH); // backlight on
  tft.fillScreen(HX8357_BLACK);
  tft.setTextSize(2);

// this sets ADC to 0-3.3V range
  analogSetPinAttenuation(DustPin, ADC_2_5db);
  dustsensor.begin(DustLEDPin, DustPin);
  dustsensor.setInputVolts(5);
  dustsensor.setADCbit(12);

// Turning off the air filter at the start of program (we only want it to be on when dust>400)
  mySwitch.send(4281660, 24);
  Serial.println("Socket 1 off");
  Serial.println("Wifi initiated");
}

void loop() {
  //Unphone power management 
  checkPowerSwitch(); // check if the power switch is now off & if so shutdown
  tft.fillRect(0,50,320,200, HX8357_BLACK); // clear screen ready for next display
  tft.setCursor(0,50);
  //Dust sensor reading
  tft.println("Dust Density:");
  float dust = dustsensor.getDustDensity();
  tft.println(dust);
  Serial.println(dust);
  onFilter(dust);
  offFilter(dust);
   delay(10000);
}

//Sending the on code
void onFilter(float dust){
    if(dust > 400){
    mySwitch.send(4281651, 24);
    Serial.println("Socket 1 on");
      //Loggin dust particles level to google sheets using IFTTT- uncomment after updating ssid & pass
      //initWifi();
      //makeIFTTTRequest();
     //delay(60000);
     delay(5000);
    }
  }
//Sending the off code 
void offFilter(float dust){
    if(dust < 365){
      mySwitch.send(4281660, 24);
      Serial.println("Socket 1 off");
      delay(5000);
    }
  }

// power management chip API /////////////////////////////////////////////////
void checkPowerSwitch() {
  uint8_t inputPwrSw = IOExpander::digitalRead(IOExpander::POWER_SWITCH);

  // bit 2 of status register indicates if USB connected
  bool powerGood = bitRead(getRegister(BM_Status),2);

  if(!inputPwrSw) {  // when power switch off
    if(!powerGood) { // and usb unplugged we go into shipping mode
      Serial.println("Setting shipping mode to true");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      setShipping(true);
    } else { // power switch off and usb plugged in we sleep
      Serial.println("sleeping now");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      esp_sleep_enable_timer_wakeup(1000000); // sleep time is in uSec
      esp_deep_sleep_start();
    }
  }
}
void setShipping(bool value) { // put the bat man into shipping mode: sleep
  byte result;
  if(value) {
    result=getRegister(BM_Watchdog); // read current state of timing register
    bitClear(result, 5);             // clear bit 5
    bitClear(result, 4);             // and bit 4
    setRegister(BM_Watchdog,result); // disable watchdog tmr (REG05[5:4] = 00)

    result=getRegister(BM_OpCon);    // current state of operational register
    bitSet(result, 5);               // set bit 5
    setRegister(BM_OpCon,result);    // to disable BATFET (REG07[5] = 1)
  } else {
    result=getRegister(BM_Watchdog); // Read current state of timing register
    bitClear(result, 5);             // clear bit 5
    bitSet(result, 4);               // and set bit 4
    setRegister(BM_Watchdog,result); // enable watchdog tmr (REG05[5:4] = 01)

    result=getRegister(BM_OpCon);    // current state of operational register
    bitClear(result, 5);             // clear bit 5
    setRegister(BM_OpCon,result);    // to enable BATFET (REG07[5] = 0)
  }
}

// I2C helpers to drive the power management chip
void setRegister(byte reg, byte value) {
  write8(I2Cadd, reg, value);
}
byte getRegister(byte reg) {
  byte result;
  result=read8(I2Cadd, reg);
  return result;
}
void write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}
byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}

// Establish a Wi-Fi connection with your router
void initWifi() {
  Serial.print("Connecting to: "); 
  Serial.print(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);  
  //WiFi.setSleep(false);// this code solves my problem
  int timeout = 10 * 4; // 10 seconds
  while(WiFi.status() != WL_CONNECTED  && (timeout-- > 0)) {
    delay(250);
    Serial.print(".");
  }
  Serial.println("");

  if(WiFi.status() != WL_CONNECTED) {
     Serial.println("Failed to connect, going back to sleep");
  }

  Serial.print("WiFi connected in: "); 
  Serial.print(millis());
  Serial.print(", IP address: "); 
  Serial.println(WiFi.localIP());
}

// Make an HTTP request to the IFTTT web service
void makeIFTTTRequest() {
  Serial.print("Connecting to "); 
  Serial.print(server);
  
  WiFiClient client;
  int retries = 5;
  while(!!!client.connect(server, 80) && (retries-- > 0)) {
    Serial.print(".");
  }
  Serial.println();
  if(!!!client.connected()) {
    Serial.println("Failed to connect...");
  }
  
  Serial.print("Request resource: "); 
  Serial.println(resource);

  // Dust level 
  String jsonObject = String("{\"value1\":\"") + dustsensor.getDustDensity() + "\",\"value2\":\"" + dustsensor.getDustDensity()
                      + "\",\"value3\":\"" + dustsensor.getDustDensity() + "\"}";
                      
                      
  client.println(String("POST ") + resource + " HTTP/1.1");
  client.println(String("Host: ") + server); 
  client.println("Connection: close\r\nContent-Type: application/json");
  client.print("Content-Length: ");
  client.println(jsonObject.length());
  client.println();
  client.println(jsonObject);
        
  int timeout = 5 * 10; // 5 seconds             
  while(!!!client.available() && (timeout-- > 0)){
    delay(100);
  }
  if(!!!client.available()) {
    Serial.println("No response...");
  }
  while(client.available()){
    Serial.write(client.read());
  }
  
  Serial.println("\nclosing connection");
  client.stop(); 
}
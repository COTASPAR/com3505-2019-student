uint64_t chipid; 
int pushButton = 14, redLED = 15, yellowLED = 32 , greenLED = 12;

void setup() {

  pinMode(pushButton,INPUT_PULLUP);
  pinMode(redLED,OUTPUT);
  pinMode(yellowLED,OUTPUT);
  pinMode(greenLED,OUTPUT);
 
}

  void blinkRed(){
  //blinking red LED:
  digitalWrite(redLED,HIGH);
  }
  void blinkYellow(){
  //blinking yellow LED:
  digitalWrite(yellowLED,HIGH);
  }
  void blinkGreen(){
  //blinking green LED:
  digitalWrite(greenLED,HIGH);
  }

// the loop function runs over and over again forever
void loop() {
 int push_state = digitalRead(pushButton);
 if( push_state == LOW){
    blinkGreen();
    delay(1000);
    blinkRed();
    delay(1000);
    blinkYellow();
    delay(1000);
    //delay(1000);
  }else{
    digitalWrite(redLED,LOW);
    digitalWrite(yellowLED,LOW);
    digitalWrite(greenLED,LOW);
    }
 

  
}
